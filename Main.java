package com.greatLearning.assignment;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// create the object of SuperDepartment and use all the methods
	    SuperDepartment sd=new SuperDepartment();
		String departmentname_=sd.deparmentName();
		System.out.println("Name of the Department:-  "+departmentname_);
		String sdtodays_Work=sd.getTodaysWork();
		System.out.println("Todays work of Super Department:-  "+sdtodays_Work);
		String Work_deadline=sd.getWorkDeadline();
		System.out.println("Super Department Works deadline:-  "+ Work_deadline);
		String superDepartmentHoliday=sd.isTodayAHoliday();
		System.out.println("Holiday status:- "+superDepartmentHoliday);
		System.out.println("------------END OF SUPER DEPARTMENT INFORMATION-------------");
		
		
		// create the object of AdminDepartment and use all the methods
		
		AdminDepartment ad = new AdminDepartment();
		String department_Name=ad.deparmentName();
		System.out.println("Name of the Department:-  "+department_Name);
		String todays_work=ad.getTodaysWork();
		System.out.println("Todays work of Admin Department:-  "+todays_work);
		String work_deadline=ad.getWorkDeadline();
		System.out.println("Admin's Department Works deadline:-  "+ work_deadline);
		String adminDepartmentHoliday=ad.isTodayAHoliday();
		System.out.println("Holiday status:- "+adminDepartmentHoliday);
		System.out.println("------------END OF ADMIN DEPARTMENT INFORMATION-------------");
		
		
		// create the object of HR Department and use all the methods
		
		HrDepartment hd = new HrDepartment();
		String department_name=hd.deparmentName();
		System.out.println("Name of the Department:-  "+department_name);
		String todays_Work=hd.getTodaysWork();
		System.out.println("Todays work of HR Department:-  "+todays_Work);
		String hr_activity=hd.doActivity();
		System.out.println("HR department current activity:-  "+hr_activity);
		String work_Deadline=hd.getWorkDeadline();
		System.out.println("HR Department works deadline:-  "+work_Deadline);
		String HRDepartmentHoliday=hd.isTodayAHoliday();
		System.out.println("Holiday status:- "+HRDepartmentHoliday);
		System.out.println("------------END OF HR DEPARTMENT INFORMATION-------------");
		
		
		// create the object of TechDepartment and use all the methods
		
		TechDepartment td = new TechDepartment();
		String _departmentName=td.deparmentName();
		System.out.println("Name of the Department:-  "+_departmentName);
		String _techdepartmentwork=td.getTodaysWork();
		System.out.println("Todays work of HR Department:-  "+_techdepartmentwork);
		String  _techstackinformation=td.getTechStackInformation();
		System.out.println("TechStack Information:-  "+_techstackinformation);
		String _workdeadline=td.getWorkDeadline();
		System.out.println("Tech Department Works deadline:-  "+ _workdeadline);
		String techDepartmentHoliday=td.isTodayAHoliday();
		System.out.println("Holiday status:- "+techDepartmentHoliday);
		System.out.println("------------END OF TECH DEPARTMENT INFORMATION-------------");

		
	}

}
